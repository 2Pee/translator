-module(translator).

-export([init/0, start/0]).

init() ->
    io:fread('HoR:', "~s").

start()->
    application:ensure_all_started(?MODULE).
