{application, 'translator', [
	{description, "New project"},
	{vsn, "0.1.0"},
	{modules, ['translator','translator_app','translator_sup']},
	{registered, [translator_sup]},
	{applications, [kernel,stdlib]},
	{mod, {translator_app, []}},
	{env, []}
]}.